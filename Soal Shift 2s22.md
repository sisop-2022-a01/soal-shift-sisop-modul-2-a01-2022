# Soal Shift Modul 2 Sistem Operasi 2022

1. Waktu pengerjaan dimulai selasa(15/3) pukul 10.00 WIB hingga sabtu(19/3) pukul 22.00 WIB.
2. Praktikan diharapkan membuat laporan penjelasan dan penyelesaian soal (lapres) dalam bentuk Readme (gitlab). Deadline lapres adalah hari terakhir demo/revisi pukul 22.00 WIB (seperti di peraturan dan timeline).
3. Format nama dan link repository gitlab "soal-shift-sisop-modul-2-XXX-2022" (contoh: https://gitlab.com/vanzeven/soal-shift-sisop-modul-2-A69-2022).
4. Struktur repository seperti berikut:
   - readme.md
   - soal1
     - soal1.c
   - soal2
     - soal2.c
   - soal3
     - soal3.c

   Jika **melanggar struktur repo** akan **dianggap sama dengan curang** dan menerima konsekuensi **sama** dengan **melakukan kecurangan**.
5. Setelah pengerjaan selesai, semua script ditaruh di gitlab masing - masing kelompok, dan link gitlab diletakkan pada form yang disediakan.
6. Commit terakhir maksimal 10 menit setelah waktu pengerjaan berakhir. Jika melewati maka akan dinilai berdasarkan commit terakhir.
7. Jika tidak ada pengumuman perubahan soal oleh asisten, maka soal dianggap dapat diselesaikan.
8. Jika ditemukan soal yang tidak dapat diselesaikan, harap menuliskannya pada Readme beserta permasalahan yang ditemukan.
9. Praktikan tidak diperbolehkan menanyakan jawaban dari soal yang diberikan kepada asisten maupun praktikan dari kelompok lainnya.
10. Jika ditemukan **indikasi kecurangan** dalam bentuk apapun di pengerjaan soal shift, maka **nilai dianggap 0**.
11. Pengerjaan soal shift sesuai dengan modul yang telah diajarkan.
12. Zip dari repository dikirim ke email asisten penguji dengan subjek yang sama dengan nama judul repository, dikirim sebelum deadline dari soal shift

## Soal 1: Gacha Simulator

Mas Refadi adalah seorang wibu gemink.  Dan jelas game favoritnya adalah Bengshin Impek. Terlebih pada game tersebut ada sistem gacha item yang membuat orang-orang selalu ketagihan untuk terus melakukan nya. Tidak terkecuali dengan mas Refadi sendiri. Karena rasa penasaran bagaimana sistem gacha bekerja, maka dia ingin membuat sebuah program untuk men-simulasi sistem history gacha item pada game tersebut. Tetapi karena dia lebih suka nge-wibu dibanding ngoding, maka dia meminta bantuanmu untuk membuatkan program nya. Sebagai seorang programmer handal, bantulah mas Refadi untuk memenuhi keinginan nya itu. 

1. Saat program pertama kali berjalan. Program akan mendownload file characters dan file weapons dari link yang ada dibawah, lalu program akan mengekstrak kedua file tersebut. File tersebut akan digunakan sebagai database untuk melakukan gacha item characters dan weapons. Kemudian akan dibuat sebuah folder dengan nama `gacha_gacha` sebagai working directory. Seluruh hasil gacha akan berada di dalam folder tersebut. Penjelasan sistem gacha ada di poin (1.4).
2. Mas Refadi ingin agar setiap kali gacha, item characters dan item weapon akan selalu bergantian diambil datanya dari database. Maka untuk setiap kali jumlah-gacha nya bernilai genap akan dilakukan gacha item weapons, jika bernilai ganjil maka item characters. Lalu untuk setiap kali jumlah-gacha nya mod 10, maka akan dibuat sebuah file baru (.txt) dan output hasil gacha selanjutnya akan berada di dalam file baru tersebut. Dan setiap kali jumlah-gacha nya mod 90, maka akan dibuat sebuah folder baru dan file (.txt) selanjutnya akan berada didalam folder baru tersebut.  **Sehingga untuk setiap folder, akan terdapat 9 file (.txt) yang didalamnya berisi 10 hasil gacha**. Dan karena ini simulasi gacha, maka hasil gacha di dalam file .txt adalah ACAK/RANDOM dan setiap file (.txt) isi nya akan **BERBEDA**
3. Format penamaan setiap file (.txt) nya adalah `{Hh:Mm:Ss}_gacha_{jumlah-gacha}.txt`, misal `04:44:12_gacha_120.txt`, dan format penamaan untuk setiap folder nya adalah `total_gacha_{jumlah-gacha}`, misal `total_gacha_270`. Dan untuk setiap file (.txt) akan memiliki perbedaan penamaan waktu output sebesar **1 second**.
4. Pada game tersebut, untuk melakukan gacha item kita harus menggunakan alat tukar yang dinamakan **primogems**. Satu kali gacha item akan menghabiskan primogems sebanyak 160 primogems. Karena mas Refadi ingin agar hasil simulasi gacha nya terlihat banyak, maka pada program, primogems di awal di-define sebanyak **79000** primogems. Setiap kali gacha, ada **2 properties** yang akan diambil dari database, yaitu name dan rarity. Lalu Outpukan hasil gacha nya ke dalam file (.txt) dengan format hasil gacha `{jumlah-gacha}_[tipe-item]_{rarity}_{name}_{sisa-primogems}.txt`. Program akan selalu melakukan gacha hingga primogems habis.
   Contoh : `157_characters_5_Albedo_53880.txt`
5. Proses untuk melakukan gacha item akan dimulai bertepatan dengan anniversary pertama kali mas Refadi bermain Bengshin Impek, yaitu pada 30 Maret jam 04:44.  Kemudian agar hasil gacha nya tidak dilihat oleh teman kos nya, maka 3 jam setelah anniversary tersebut semua isi di folder `gacha_gacha` akan di zip dengan nama `not_safe_for_wibu` dengan dipassword `satuduatiga`, lalu semua folder akan di delete sehingga hanya menyisakan file (.zip)

Note:

- Menggunakan fork dan exec.
- Tidak boleh menggunakan fungsi system(), mkdir(), dan rename().
- Tidak boleh pake cron.
- Semua poin dijalankan oleh 1 script di latar belakang. Cukup jalankan script 1x serta ubah time dan date untuk check hasilnya.
- Link
  - Database item characters: https://drive.google.com/file/d/1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp/view
  - Database item weapons: https://drive.google.com/file/d/1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT/view

Tips:

- Gacha adalah proses untuk mendapatkan suatu item dengan cara melakukan randomize dari seluruh item yang ada.
- Dikarenakan file database memiliki format (.json). Silahkan gunakan library <json-c/json.h>, install dengan “apt install libjson-c-dev”, dan compile dengan “gcc [nama_file] -l json-c -o [nama_file_output]”. Silahkan gunakan dokumentasi berikut untuk membaca dan parsing file (.json): https://progur.com/2018/12/how-to-parse-json-in-c.html 

## Soal 2: Film Review

Japrun bekerja di sebuah perusahaan dibidang review industri perfilman, karena kondisi saat ini sedang pandemi Covid-19, dia mendapatkan sebuah proyek untuk mencari drama korea yang tayang dan sedang ramai di Layanan Streaming Film untuk diberi review. Japrun sudah mendapatkan beberapa foto-foto poster serial dalam bentuk zip untuk diberikan review, tetapi didalam zip tersebut banyak sekali poster drama korea dan dia harus memisahkan poster-poster drama korea tersebut tergantung dengan kategorinya. Japrun merasa kesulitan untuk melakukan pekerjaannya secara manual, kamu sebagai programmer diminta Japrun untuk menyelesaikan pekerjaannya.

1. Hal pertama yang perlu dilakukan oleh program adalah meng-*extract* zip yang diberikan ke dalam folder `/home/[user]/shift2/drakor`. Karena atasan Japrun teledor, dalam zip tersebut bisa berisi folder-folder yang tidak penting, maka program harus bisa membedakan file dan folder sehingga dapat memproses file yang seharusnya dikerjakan dan menghapus folder-folder yang tidak dibutuhkan.

2. Poster drama korea perlu dikategorikan sesuai jenisnya, maka program harus membuat folder untuk setiap jenis drama korea yang ada dalam zip. Karena kamu tidak mungkin memeriksa satu-persatu manual, maka program harus membuatkan folder-folder yang dibutuhkan sesuai dengan isi zip.
   
   Contoh: Jenis drama korea romance akan disimpan dalam `drakor/romance`, jenis drama korea action akan disimpan dalam `drakor/action` , dan seterusnya.

3. Setelah folder kategori berhasil dibuat, program akan memindahkan poster ke folder dengan kategori yang sesuai dan di rename dengan nama.
   
   Contoh: `drakor/romance/start-up.png`.

4. Karena dalam satu foto bisa terdapat lebih dari satu poster maka foto harus di pindah ke masing-masing kategori yang sesuai.
   Contoh: foto dengan nama `start-up;2020;romance_the-k2;2016;action.png` dipindah ke folder `drakor/romance/start-up.png` dan `drakor/action/the-k2.png`.

5. Di setiap folder kategori drama korea buatlah sebuah file `data.txt` yang berisi nama dan tahun rilis semua drama korea dalam folder tersebut, jangan lupa untuk sorting list serial di file ini berdasarkan tahun rilis (*ascending*). Format harus sesuai contoh dibawah ini.
   
   ```yaml
   kategori : romance
   
   nama : start-up
   rilis  : tahun 2020
   
   nama : twenty-one-twenty-five
   rilis  : tahun 2022
   ```

Note dan Ketentuan Soal:

- File zip berada dalam drive modul shift ini bernama `drakor.zip`
- File yang penting hanyalah berbentuk `.png`
- Setiap foto poster disimpan sebagai nama foto dengan format `[nama]:[tahun rilis]:[kategori]`. Jika terdapat lebih dari satu drama dalam poster, dipisahkan menggunakan underscore `_`.
- Tidak boleh menggunakan fungsi `system()`, `mkdir()`, dan `rename()` yang tersedia di bahasa C.
- Gunakan bahasa pemrograman C (Tidak boleh yang lain).
- Folder shift2, drakor, dan kategori dibuatkan oleh program (Tidak Manual).
- `[user]` menyesuaikan nama user linux di os anda.

## Soal 3: Private Detective

Conan adalah seorang detektif terkenal. Suatu hari, Conan menerima beberapa laporan tentang hewan di kebun binatang yang tiba-tiba hilang. Karena jenis-jenis hewan yang hilang banyak, maka perlu melakukan klasifikasi hewan apa saja yang hilang

1. Untuk mempercepat klasifikasi, Conan diminta membuat program untuk membuat 2 directory di `/home/[USER]/modul2/` dengan nama `darat` lalu 3 detik kemudian membuat directory ke 2 dengan nama `air`. 
2. Kemudian program diminta dapat melakukan extract “`animal.zip`” di “`/home/[USER]/modul2/`”.
3. Tidak hanya itu, hasil extract dipisah menjadi hewan darat dan hewan air sesuai dengan nama filenya. Untuk hewan darat dimasukkan ke folder “`/home/[USER]/modul2/darat`” dan untuk hewan air dimasukkan ke folder “`/home/[USER]/modul2/air`”. Rentang pembuatan antara folder darat dengan folder air adalah 3 detik dimana folder darat dibuat terlebih dahulu. Untuk hewan yang tidak ada keterangan air atau darat harus dihapus.
4. Setelah berhasil memisahkan hewan berdasarkan hewan darat atau hewan air. Dikarenakan jumlah burung yang ada di kebun binatang terlalu banyak, maka pihak kebun binatang harus merelakannya sehingga conan harus menghapus semua burung yang ada di directory “`/home/[USER]/modul2/darat`”. Hewan burung ditandai dengan adanya “`bird`” pada nama file.
5. Terakhir, Conan harus membuat file list.txt di folder “`/home/[USER]/modul2/air`” dan membuat list nama semua hewan yang ada di directory “`/home/[USER]/modul2/air`” ke “`list.txt`” dengan format `UID_[UID file permission]_Nama File.[jpg/png]` dimana UID adalah user dari file tersebut file permission adalah permission dari file tersebut.
   Contoh : `conan_rwx_hewan.png`

Catatan :

- Tidak boleh memakai `system()`.
- Tidak boleh memakai function C `mkdir()` ataupun `rename()`.
- Gunakan `exec` dan `fork`
- Direktori `.` dan `..` tidak termasuk
